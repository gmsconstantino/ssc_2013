package Proxy;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.nio.ByteBuffer;
import java.security.Key;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Properties;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import Server.CipherSuiteManager;
import Utils.Utils;
import Utils.Packet;

/**
 * This class is intended to manage the entire proxy operations, these include
 * authentication and receiving the stream from the StreamServer.
 * 
 * @author miguel
 * 
 */
public class Proxy {
	
	private static final int HEADERSIZE = 10;
	private static final int NSEQUENCE_SESSIONID = 12;
	private static final int HEADERSTREAM = 10;

	private String receivedIPMulticast;
	private int receivedPortMulticast;
	private String sendIPMulticast;
	private int sendPortMulticast;
	private CipherSuiteManager cipherManager;
	private FileOutputStream fileOuputStream;

	private MulticastSocket rs;
	private DatagramPacket p;

	// Buffer and array of received data from udp packet
	private ByteBuffer bbReceived;
	private byte[] dataReceived;

	// Buffer and array of decipher data
	private ByteBuffer bbData;
	private byte[] buffer;
	private int expectSequenceNumber;

	// Cipher Class
	private SecretKeySpec   key;
	private IvParameterSpec ivSpec;
	private Cipher cipher;

	// Hmac Class
	private byte[] keyHmac;
	private Mac hMac;
	private Key hMacKey;
	private static byte [] salt;
	private static String userName;
	private long sessionID;

	public Proxy( CipherSuiteManager cipher, String sendIPMulticast, int sendPortMulticast) {
		super();
		this.receivedIPMulticast = cipher.getProperty("ipMultiCast");
		this.receivedPortMulticast = Integer.valueOf(cipher.getProperty("port"));
		this.sendIPMulticast = sendIPMulticast;
		this.sendPortMulticast = sendPortMulticast;

		this.cipherManager = cipher;
		sessionID = ByteBuffer.wrap(Utils.hexStringToByteArray(cipherManager.getProperty("sessionId"))).getLong();
		
		needSaveStream = false;
		initialiazeCipher();
		initializeHmac();

		expectSequenceNumber = 0;
		dataReceived = new byte[65536];
		bbReceived = ByteBuffer.wrap(dataReceived);

		buffer = new byte[65536];
		bbData = ByteBuffer.wrap(buffer);
	}

	/**
	 * Constructor to Save stream to file
	 * @param cipherManager
	 */
	public Proxy(CipherSuiteManager cipherManager) {
		this.cipherManager = cipherManager;
		sessionID = ByteBuffer.wrap(Utils.hexStringToByteArray(cipherManager.getProperty("sessionId"))).getLong();
		
		needSaveStream = true;
		initialiazeCipher();
		initializeHmac();

		expectSequenceNumber = 0;
		dataReceived = new byte[65536];
		bbReceived = ByteBuffer.wrap(dataReceived);

		buffer = new byte[65536];
		bbData = ByteBuffer.wrap(buffer);
	}


	public void run(){
		try {
			InetAddress group = InetAddress.getByName(receivedIPMulticast);

			if (!group.isMulticastAddress()) {
				System.err.println("Multicast address required...");
				System.exit(0);
			}
			rs = new MulticastSocket(receivedPortMulticast);
			rs.joinGroup(group);
			p = new DatagramPacket(dataReceived, 65536 );


			if(needSaveStream){
				fileOuputStream = new FileOutputStream(nameFileStream);
				saveStreamToFile();
				fileOuputStream.close();
			}else{
                System.out.println("Start to re-send stream");
                reSendStream();
			}
			// rs.leave if you want leave from the multicast group ...
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	private void reSendStream() throws ShortBufferException, IllegalStateException, IOException{
		DatagramSocket dgsocket = new DatagramSocket();
		InetSocketAddress epaddr = new InetSocketAddress( sendIPMulticast, sendPortMulticast);
		DatagramPacket dgpacket = new DatagramPacket(buffer, buffer.length, epaddr );
		
		do {//test
			bbReceived.position(0);
			bbData.position(0);
			
			p.setLength(65536); // resize with max size
			rs.receive(p);
			if(p.getLength()==0)
				break;

			if(bbReceived.get()!=Packet.VERSION){
				continue;
			}
			if(bbReceived.get()!=Packet.CONTENTTYPE6)
				continue;

			int size = bbReceived.getInt();
			
			if(bbReceived.getInt() < expectSequenceNumber)
				continue;

			// decipher data go to array buffer
//			int length = doDecipher(p.getData(), 10, size);
			int length = 0;
			try{
				length = doDecipher(p.getData(), HEADERSIZE, size);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if(bbData.getInt() < expectSequenceNumber)
				continue;
			else
				expectSequenceNumber++;
			
			long session = bbData.getLong();
			if(sessionID != session)
				continue;
			
			// check HMAC
			byte[] hash = Arrays.copyOfRange(buffer, length - hMac.getMacLength(), length);
			byte[] computed = doHmac(buffer, 0, length - hMac.getMacLength());
			boolean correctHMAC = MessageDigest.isEqual(hash, computed);
			if (!correctHMAC)
				continue;

			int lengthWrite = length-hMac.getMacLength()-NSEQUENCE_SESSIONID-HEADERSTREAM;
			dgpacket.setData(buffer, NSEQUENCE_SESSIONID + HEADERSTREAM, lengthWrite);
			dgpacket.setSocketAddress(epaddr);
			
			dgsocket.send( dgpacket );
			
//			System.out.print( "." );
		} while (p.getLength() > 0);
		
		dgsocket.close();
		System.out.println("Done.");
	}

	private void saveStreamToFile() throws IOException, ShortBufferException, IllegalBlockSizeException, BadPaddingException{
		do {//test
			bbReceived.position(0);
			bbData.position(0);
			
			p.setLength(65536); // resize with max size
			rs.receive(p);
			if(p.getLength()==0)
				break;

			if(bbReceived.get()!=Packet.VERSION){
				continue;
			}
			if(bbReceived.get()!=Packet.CONTENTTYPE5)
				continue;

			int size = bbReceived.getInt();
			
			if(bbReceived.getInt() < expectSequenceNumber)
				continue;

			// decipher data go to array buffer
//			int length = doDecipher(p.getData(), 10, size);
			int length = 0;
			try{
				length = doDecipher(p.getData(), HEADERSIZE, size);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if(bbData.getInt() < expectSequenceNumber)
				continue;
			else
				expectSequenceNumber++;
			
			long session = bbData.getLong();
			if(sessionID != session)
				continue;
			
			// check HMAC
			byte[] hash = Arrays.copyOfRange(buffer, length - hMac.getMacLength(), length);
			byte[] computed = doHmac(buffer, 0, length - hMac.getMacLength());
			boolean correctHMAC = MessageDigest.isEqual(hash, computed);
			if (!correctHMAC)
				continue;

			int lengthWrite = length-hMac.getMacLength()-NSEQUENCE_SESSIONID;
			fileOuputStream.write(buffer, NSEQUENCE_SESSIONID, lengthWrite);
			fileOuputStream.flush();
//			System.out.print( "." );
		} while (p.getLength() > 0);
	}

	private int doDecipher(byte[] data, int pos, int length) throws ShortBufferException, IllegalBlockSizeException, BadPaddingException {
		int ctLength = cipher.update(data, pos, length, buffer, 0);
		ctLength += cipher.doFinal(buffer, ctLength);

		return ctLength;
	}

	private byte[] doHmac(byte[] data, int offset, int length) throws ShortBufferException, IllegalStateException{
		byte[] hash = new byte[hMac.getMacLength()];
		// Process the data
		hMac.update(data, offset, length);

		// Save digest to array at offset
		hMac.doFinal(hash, 0);
		
		return hash;
	}

	private void initializeHmac(){
		try {
			// HMAC Cipher
			keyHmac = Utils.hexStringToByteArray(cipherManager.getProperty("hmacKey"));
			hMac = Mac.getInstance(cipherManager.getProperty("hmacAlgorithm"), cipherManager.getProperty("provider"));
			hMacKey = new SecretKeySpec(keyHmac, cipherManager.getProperty("hmacAlgorithm"));

			hMac.init(hMacKey);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initialiazeCipher(){
		try {
			byte[] keyBytes = Utils.hexStringToByteArray(cipherManager.getProperty("key"));
			byte[] ivBytes = Utils.hexStringToByteArray(cipherManager.getProperty("iv"));

			key = new SecretKeySpec(keyBytes, cipherManager.getAlgoritm());
			ivSpec = new IvParameterSpec(ivBytes);
			cipher = Cipher.getInstance(cipherManager.getAlgoritm(), cipherManager.getProperty("provider"));
			cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * ######## Main ###########
	 */

	private static final String WELCOME_MESSAGE = "*************WELCOME TO FCT STREAMING*************";
	// private static final String userDataRequestMessage =
	// "*****Please provide your username + password******";
	private static final String LoggingMessage = "*****Logging in******";
	private static final String ProxyParametersMessage = "usage: java Proxy authserver_ip authserver_port [multicast_ip multicast_port]\n       java Proxy authserver_ip auth_port [-copy localfile.dat]";
	private static final String NotAuthenticatedMessage = "User is not authenticated, exiting.";
	private static final String InsufficientPermissionsMessage = "User does not have enough permissions, exiting.";

	private static String password;
	private static boolean needSaveStream = false;
	private static String nameFileStream;
	private static AuthClient authClient;
	private static String authserver_ip;
	private static int authserver_port;
	private static String multicast_ip;
	private static int multicast_port;

	public static void main(String[] args) throws Exception {
		if (args.length < 4) {
			System.err.println(ProxyParametersMessage);
			System.exit(0);
		} else if(args[2].equalsIgnoreCase("-copy") || args[2].equalsIgnoreCase("-c") ){
			needSaveStream = true;
			nameFileStream = args[3];
		} else {
			multicast_ip = args[2];
			multicast_port = Integer.parseInt(args[3].trim());
		}
		
		System.out.println(WELCOME_MESSAGE);
		authserver_ip = args[0];
		authserver_port = Integer.parseInt(args[1].trim());
		
		getUserData();

		System.out.println("Estabilished connection to AuthServer");
		authClient = new AuthClient(authserver_ip, authserver_port);
		
		System.out.println("Using userName: " + userName + " password: " + password);
		int code = authClient.authenticate(userName, password);
		System.out.println("Reply code: " + code);

		if(code == 1){
			System.out.println("All authentication steps completed sucessfully, initiating.");
		
			Properties prop = authClient.getProperties();

			// Received Stream
			CipherSuiteManager cipherManager = null;
			Proxy proxy = null;
			if(needSaveStream){
				cipherManager = new CipherSuiteManager();
				// Just save Stream
				proxy = new Proxy(cipherManager);
			}else{
				cipherManager = new CipherSuiteManager(prop);
				System.out.println(LoggingMessage);
				proxy = new Proxy(cipherManager, multicast_ip, multicast_port);
			}
			
			proxy.run();
		}else{
			System.out.println("Authentication Failed.");
		}
		
		
	}
	
	public static void getUserData(){
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please type your userName:");
		userName = keyboard.next();
		System.out.println("Please type your password:");
		password = keyboard.next();
		keyboard.close();
	}

}
