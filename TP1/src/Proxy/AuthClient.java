package Proxy;

import Utils.NounceChecker;
import Utils.Packet;
import Utils.Utils;

import javax.crypto.Mac;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.nio.ByteBuffer;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateKey;
import java.util.Properties;

public class AuthClient {

	private static final String SSL_PROPERTIES = "configSSL.properties";

	SocketFactory sf;
	SSLSocket sslSocket;
	private Properties prop;
	private String[] enableCipher;
	private KeyStore keystore;
	private Mac hMac;

    private InputStream in;
    private OutputStream out;
    private DataOutputStream dos;
    private DataInputStream dis;
    private long nouceAuthServer;
    private long nounceClient;

    private NounceChecker nounceChecker;

	public AuthClient(String authServer, int authServerPort)
			throws IOException, KeyStoreException, NoSuchAlgorithmException,
			CertificateException {
		readConfig();

		enableCipher = prop.getProperty("enableCiphersuites").split(",");

        if(prop.getProperty("authType").trim().equals("2Way")){
            System.setProperty("javax.net.ssl.keyStore",
            prop.getProperty("cKeystore"));
            System.setProperty("javax.net.ssl.keyStorePassword",
            prop.getProperty("cKeyPassword"));
        }

		System.setProperty("javax.net.ssl.trustStore",
				prop.getProperty("cTruststore"));
		System.setProperty("javax.net.ssl.trustStorePassword",
				prop.getProperty("cTrustPassword"));

//        System.setProperty("javax.net.debug", "ssl");


		sf = SSLSocketFactory.getDefault();
		sslSocket = (SSLSocket) sf.createSocket(authServer, authServerPort);
		sslSocket.setEnabledCipherSuites(enableCipher);
		initializeKeyStore();

        nounceChecker = NounceChecker.getInstance();
	}

	/*
	 * Returns a hash generated from a password to be sent in step 3, this hash
	 * is compared in the server aswell (mostly to make use of the existing
	 * mechanisms.
	 */
	private byte[] generateHashFromPassword(String pwd)
			throws NoSuchProviderException, NoSuchAlgorithmException,
			InvalidKeyException, UnrecoverableEntryException, KeyStoreException {
		hMac = Mac.getInstance("HMacSHA1", "BC");
		// A chave Publica deve ser a do seu certificado da keystore
		// O ficheiro dos utilizadores passa a ter o hash feito com a chave
		// acima.
		hMac.init(getPublicKey());
		hMac.update(pwd.getBytes());
		return hMac.doFinal();
	}

	private void initializeKeyStore() throws KeyStoreException,
			NoSuchAlgorithmException, CertificateException, IOException {
		keystore = KeyStore.getInstance(KeyStore.getDefaultType());
		// get user password and file input stream
		char[] password = prop.getProperty("cKeyPassword").toCharArray();
		java.io.FileInputStream fis = null;
		try {
			fis = new java.io.FileInputStream(prop.getProperty("cKeystore"));
			keystore.load(fis, password);
		} finally {
			if (fis != null) {
				fis.close();
			}
		}
	}

	private RSAPrivateKey getPrivateKey() throws UnrecoverableEntryException,
			NoSuchAlgorithmException, KeyStoreException {
		KeyStore.ProtectionParameter protParam = new KeyStore.PasswordProtection(
				"seguranca".toCharArray());
		// get my private key
		KeyStore.PrivateKeyEntry pkEntry = (KeyStore.PrivateKeyEntry) keystore
				.getEntry("mykeys", protParam);

		PrivateKey myPrivateKey = pkEntry.getPrivateKey();
		return (RSAPrivateKey) myPrivateKey;
	}

	private PublicKey getPublicKey() throws UnrecoverableEntryException,
			NoSuchAlgorithmException, KeyStoreException {
		KeyStore.ProtectionParameter protParam = new KeyStore.PasswordProtection(
				"seguranca".toCharArray());
		// get my private key
		KeyStore.PrivateKeyEntry pkEntry = (KeyStore.PrivateKeyEntry) keystore
				.getEntry("mykey", protParam);

		return pkEntry.getCertificate().getPublicKey();
	}

	private void readConfig() {
		FileInputStream in = null;
		try {
			in = new FileInputStream(SSL_PROPERTIES);
			prop = new Properties();
			prop.load(new FileInputStream(SSL_PROPERTIES));
		} catch (FileNotFoundException e) {
			System.out.println("Can't find file.");
			System.exit(0);
		} catch (IOException e) {
			// e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public int authenticate(String userName, String password) {
		try {
			in = sslSocket.getInputStream();
			out = sslSocket.getOutputStream();

            dis = new DataInputStream(in);
            dos = new DataOutputStream(out);

            step1(userName, password);
            receive();
            step3();

		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}

		return 1;
	}

    /*
    Send P->A : Na, Username, H(PWD)
     */
    private void step1(String userName, String password) throws IOException{
        byte[] buffer = new byte[65000];
        ByteBuffer bb = ByteBuffer.wrap(buffer);
        int size = 0;

        bb.put(Packet.VERSION);
        bb.put(Packet.CONTENTTYPE1);
        // Reserve space to size package
        bb.putInt(1);

        boolean nnew = true;
        do{
            nounceClient = Packet.getNounce();
            nnew = nounceChecker.checkNouce(nounceClient);
        }while (nnew);
        bb.putLong(nounceClient);
        size += 8;


        int len = userName.getBytes().length;
        bb.putInt(len);
        bb.put(userName.getBytes());
        size += 4 + len;

        String hash = null;
        try {
            hash = Utils.toHex(generateHashFromPassword(password));
        } catch (Exception e) {
            throw new IOException();
        }
        len = hash.getBytes().length;
        bb.putInt(len);
        bb.put(hash.getBytes());
        size += 4 + len;

        bb.putInt(2, size);
        dos.write(buffer, 0, size+6);
        System.out.println("Send User&Pass");
    }

    /*
    Send P->A : {Nc+1,<OK_FLAG>}ks
     */
    private void step3() throws IOException{
        byte[] buffer = new byte[65000];
        ByteBuffer bb = ByteBuffer.wrap(buffer);
        int size = 0;

        bb.put(Packet.VERSION);
        bb.put(Packet.CONTENTTYPE3);
        // Reserve space to size package (int 4 bytes)
        bb.putInt(1);
        //8 bytes long
        nouceAuthServer++;
        bb.putLong(nouceAuthServer);

        size += 8;

        //<OK>
        bb.putInt(1);
        size += 4;

        bb.putInt(2, size);
        dos.write(buffer, 0, size+6);
        System.out.println("Send <OK>");
    }

    /*
   Receive A->P : {Nc+1,Nd,<Ciphersuite>,H(M)}ks
    */
    private void receive() throws IOException{
        if (dis.readByte() != Packet.VERSION) {
            sslSocket.close();
            return;
        }

        if(dis.readByte() != Packet.CONTENTTYPE2){
            sslSocket.close();
            return;
        }

        nounceClient++;

        //Dados recebidos
        int size = dis.readInt();
        byte[] data = new byte[size];
        in.read(data);
        ByteBuffer bb = ByteBuffer.wrap(data);

        long nouncecPlus1 = bb.getLong();
        nouceAuthServer = bb.getLong();

        if (nounceChecker.checkNouce(nouceAuthServer)){
            sslSocket.close();
            return;
        }

        //Comparacao Nounces
        if (nounceClient == nouncecPlus1) {
            int stringSize = bb.getInt();
            byte[] dest = new byte[stringSize];
            bb.get(dest, 0, stringSize);
            String stringProperties = new String(dest);
            String[] propertiesArray = stringProperties.split(";");
            propertiesCreate(propertiesArray);
            System.out.println("Received Ciphersuite");
        } else {
            System.out.println("Nounce Mismatch");
            throw new IOException("Step 2");
        }
    }

	public Properties getProperties() {
		return prop;
	}

	private void propertiesCreate(String[] propertiesArray) {
		prop = new Properties();
		for (String aPropertiesArray : propertiesArray) {
			String[] temp = aPropertiesArray.split(":");
			prop.setProperty(temp[0], temp[1]);
		}
	}
}
