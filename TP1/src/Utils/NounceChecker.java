package Utils;

import java.util.HashSet;

/**
 * Project: ssc_2013
 * User: Gomes
 * Date: 14/12/13
 * Time: 11:27
 * <p/>
 * TODO: Class description
 */
public class NounceChecker {

    private static NounceChecker instance;

    private HashSet<Long> nounces;

    private NounceChecker() {
        nounces = new HashSet<Long>();
    }

    public static NounceChecker getInstance(){
        if (instance == null) {
            instance = new NounceChecker();
        }
        return instance;
    }

    /*
     * If return true the nounce has be used before
     */
    public boolean checkNouce(Long n){
        return !nounces.add(n);
    }


}
