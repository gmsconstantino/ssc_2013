package Utils;

import java.nio.ByteBuffer;
import java.security.SecureRandom;
/**
 * This packet should be changed immediatly, it confuses client and server concepts.
 * @author miguel
 *
 */
public class Packet {
	//public class, CHANGE WHEN POSSIBLE
	public static final byte VERSION = 0x01;
	// Content-type package for first package authentication
	public static final byte CONTENTTYPE1 = 0x01;
	// Content-type package for second package authentication
	public static final byte CONTENTTYPE2 = 0x02;
	// Content-type package for third package authentication
	public static final byte CONTENTTYPE3 = 0x03;
	// Content-type package for fourth package authentication
	public static final byte CONTENTTYPE4 = 0x04;
    // Content-type package for fifth package authentication
    public static final byte CONTENTTYPE5 = 0x05;
	// Content-type package for streaming
	public static final byte CONTENTTYPE6 = 0x06;


	public static long getNounce(){
		SecureRandom sr = new SecureRandom();
		byte[] output = new byte[8];
		sr.nextBytes(output);
		return ByteBuffer.wrap(output).getLong();
	}

}
