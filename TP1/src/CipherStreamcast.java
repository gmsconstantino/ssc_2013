import java.io.DataInputStream;
import java.io.FileInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.security.Key;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import Server.CipherSuiteManager;
import Utils.Utils;
import Utils.Packet;

/**
 * This class send the stream encapsulated on the packet type of stream
 * @author constantino
 *
 */

class CipherStreamcast extends Thread{
	
	private static final int HEADERSIZE = 10;
	
	private String file;
	private String multicastIp;
	private int port;
	private CipherSuiteManager cipherManager;
	
	private ByteBuffer bb;
	private byte[] buffer;
	
	// Cipher Class
	private SecretKeySpec   key;
	private IvParameterSpec ivSpec;
	private Cipher cipher;
    
    // Hmac Class
	private byte[] keyHmac;
	private Mac hMac;
	private Key hMacKey;

	public CipherStreamcast(String file) {
		super();
		this.file = file;
		
		cipherManager = new CipherSuiteManager();
		this.multicastIp = cipherManager.getProperty("ipMultiCast");
		this.port = Integer.valueOf(cipherManager.getProperty("port"));
		
		buffer = new byte[65000];
		bb = ByteBuffer.wrap(buffer);
		
		//loads the configuration properties
	}

	public void run() {    	
		int pos;
		int size;
		int blockcount = 0;
		long marktime;
		DataInputStream dstream;
		try {
			dstream = new DataInputStream( new FileInputStream(file) );

			// To send the stream, it is only necessary to have a Datagram socket
			// You must remember that because we will not receive nothing in
			// multicast, a datagram socket is ok.

			DatagramSocket dgsocket = new DatagramSocket();
			InetSocketAddress epaddr = new InetSocketAddress( multicastIp, port);
			DatagramPacket dgpacket = new DatagramPacket(buffer, buffer.length, epaddr );
			
			initialiazeCipher();
			initializeHmac();
			
			long time0 = System.nanoTime(); // Iinitial ref time
			long qmark0 = 0;
			
			long sessionId = ByteBuffer.wrap(Utils.hexStringToByteArray(cipherManager.getProperty("sessionId"))).getLong();
			
			bb.put(Packet.VERSION);
			// Content type Stream
			bb.put(Packet.CONTENTTYPE6);
			// Reserve space to size package
			bb.putInt(1);
			
			System.out.println("Streaming ...");
			while ( dstream.available() > 0 ) {
				pos = 0;
				bb.position(6);
				
				// Numero de sequencia fora da cifra
				bb.putInt(blockcount);

				// Numero de sequencia cifrado
				bb.putInt(blockcount);
				pos += 4;
				
				// SessionId
				bb.putLong(sessionId);
				pos += 8;
				
				// Informacao do video
				size = dstream.readShort();
				bb.putShort((short)size);
				pos += 2;
				
				marktime = dstream.readLong();
				bb.putLong(marktime);
				pos += 8;
				
				if ( blockcount == 0 ) qmark0 = marktime; // stream format ref. time
				blockcount += 1;
				dstream.readFully(buffer, HEADERSIZE+pos, size );
				
				// doHmac(byte[] data, int offset, int length, int offsetSave)
				doHmac(buffer, HEADERSIZE, pos+size, HEADERSIZE+pos+size);
//				byte[] hash = Arrays.copyOfRange(buffer, HEADERSIZE+pos+size, HEADERSIZE+pos+size+hMac.getMacLength());

				//doCipher(byte[] data, int pos, int length)
				int length = doCipher(buffer, HEADERSIZE, pos+size+hMac.getMacLength());
				
				// Set Size
				bb.putInt(2, length);
				
				dgpacket.setData(buffer, 0, HEADERSIZE+length );
				
				long time1 = System.nanoTime(); //time in this moment
				Thread.sleep(Math.max(0,((marktime-qmark0)-(time1-time0))/1000000));
				
//				System.out.println("Packet: "+Utils.toHex(buffer,HEADERSIZE+length));
				
				dgsocket.send( dgpacket );
//				System.out.print( "." );
			}
			
			dgpacket.setLength(0);
			dgsocket.send( dgpacket );
			dgsocket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("All stream blocks sent, Nr of blocks: " + blockcount);
	}
	
	

	private void doHmac(byte[] data, int offset, int length, int offsetSave) throws ShortBufferException, IllegalStateException{
		
		// Process the data
		hMac.update(data, offset, length);
		
		// Save digest to array at offset
		hMac.doFinal(buffer, offsetSave);
	}
	
	private int doCipher(byte[] data, int pos, int length) throws ShortBufferException, IllegalBlockSizeException, BadPaddingException {
		// Cipher data[]
		byte[] cipherText = new byte[cipher.getOutputSize(length)];
        int ctLength = cipher.update(buffer, pos, length, cipherText, 0);
        ctLength += cipher.doFinal(cipherText, ctLength);
        
		// Save to 
        System.arraycopy(cipherText, 0, buffer, pos, ctLength);
        
        return ctLength;
	}
	
	private void initializeHmac(){
		try {
			// HMAC Cipher
			keyHmac = Utils.hexStringToByteArray(cipherManager.getProperty("hmacKey"));
			hMac = Mac.getInstance(cipherManager.getProperty("hmacAlgorithm"), cipherManager.getProperty("provider"));
			hMacKey = new SecretKeySpec(keyHmac, cipherManager.getProperty("hmacAlgorithm"));

			hMac.init(hMacKey);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void initialiazeCipher(){
		try {
			byte[] keyBytes = Utils.hexStringToByteArray(cipherManager.getProperty("key"));
			byte[] ivBytes = Utils.hexStringToByteArray(cipherManager.getProperty("iv"));

			key = new SecretKeySpec(keyBytes, cipherManager.getAlgoritm());
			ivSpec = new IvParameterSpec(ivBytes);
			cipher = Cipher.getInstance(cipherManager.getAlgoritm(), cipherManager.getProperty("provider"));
			cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	static public void main( String []args ) throws Exception {
		// Use: args[0] the stream. file
		// args[1] to give the multicast group address
		// args[2[ to give the used port
		CipherStreamcast st = new CipherStreamcast(args[0]);
		st.run();
	}

}