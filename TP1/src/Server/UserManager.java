package Server;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.TreeMap;

import Utils.Pair;


public class UserManager {

	private TreeMap<String, Pair<String, String>> users;
	private static final String FILEAUTHUSERS = "AuthUsers.txt";

	public UserManager(){
		users = new TreeMap<String, Pair<String, String>>();
		if(!loadUsersAuth())
			System.exit(-1);
		
	}
	public boolean userNameExists(String userName){
		if(users.containsKey(userName))
			return true;
		else
			return false;
	}
	public String getSalt(String user){
		return users.get(user).getKey();
	}

	public String getPassword(String user) {
		return users.get(user).getValue();
	}

	private boolean loadUsersAuth() {
		File file = new File(FILEAUTHUSERS);
		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(file);
			while (fileScanner.hasNextLine()) {
				String line = fileScanner.nextLine();
				String[] lineData = line.split(":");
				Pair<String, String> p = new Pair<String, String>(lineData[1], lineData[2]);
				users.put(lineData[0], p);
			}
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			fileScanner.close();
		}		
		return false;
	}
}
