package Server;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.Properties;
import java.util.Scanner;

import javax.crypto.KeyGenerator;


import Utils.Utils;
import Utils.Packet;

/**
 * 
 * @author David
 * Classe responsavel por ler o ficheiro Config file, ou gerar um Config file default.
 */


public class CipherSuiteManager {
	
	private Properties prop;
	private static final String CIPHER_PROPERTIES= "ciphersuite.properties";
	
	public CipherSuiteManager(){
		loadConfigFile();
	}
	
	public CipherSuiteManager(Properties prop){
		this.prop = prop;
	}
	
	public String getAlgoritm(){
		return prop.getProperty("algorithm")+"/"+prop.getProperty("mode")+
				"/"+prop.getProperty("padding");
	}
	
	public String getProperty(String p){
		return prop.getProperty(p);
	}
	//Leitura do ficheiro 
	private boolean loadConfigFile(){
		FileInputStream in = null;
		try{
			in = new FileInputStream(CIPHER_PROPERTIES);
			prop = new Properties();
    		prop.load(new FileInputStream(CIPHER_PROPERTIES));
		} catch (FileNotFoundException e) {
			System.out.println("Configuration File not found!");
			Scanner keyboard = new Scanner(System.in);
			System.out.println("Would you like us to create the configuration file? (YES/NO)");
			String createConf = keyboard.nextLine();
			if(createConf.toLowerCase().equals("yes") || createConf.toLowerCase().equals("y"))
				try {
					createConfigFile();
				} catch (NoSuchAlgorithmException e1) {
					e1.printStackTrace();
				} catch (NoSuchProviderException e1) {
					e1.printStackTrace();
				}
			keyboard.close();
			System.exit(0);
		} catch (IOException e) {
			// e.printStackTrace();
		}finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	private void createConfigFile() throws NoSuchAlgorithmException, NoSuchProviderException{
		SecureRandom random = new SecureRandom();
		Properties prop = new Properties();		
    	try {
    		prop.setProperty("provider", "BC");
    		prop.setProperty("algorithm", "DES");
    		prop.setProperty("mode", "CBC");
    		prop.setProperty("padding", "PKCS5Padding");
    		prop.setProperty("ksSize", "56");
    		prop.setProperty("key", randomKeyGenerator(prop));
    		prop.setProperty("iv", Long.toHexString(Packet.getNounce()));
    		prop.setProperty("hmacAlgorithm", "HMacSHA1");
    		prop.setProperty("khmacSize", "256");
    		prop.setProperty("hmacKey", Utils.toHex(Utils.createKeyForAES(256, random).getEncoded()));
    		prop.setProperty("sessionId", Long.toHexString(random.nextLong()));
    		
    		prop.store(new FileOutputStream(CIPHER_PROPERTIES), null);
    		System.out.println("Configuration file created.");
    	} catch (IOException ex) {
    		ex.printStackTrace();
        }
	}
	
	public void setNewSessionId(){
		SecureRandom sr = new SecureRandom();
		ByteBuffer bb = ByteBuffer.allocate(8);
		bb.putLong(sr.nextLong());
		prop.setProperty("sessionId", Utils.toHex(bb.array()));
		try {
			prop.store(new FileOutputStream(CIPHER_PROPERTIES), null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String randomKeyGenerator(Properties p){
		KeyGenerator generator;
		try {
			generator = KeyGenerator.getInstance(p.getProperty("algorithm"), 
					p.getProperty("provider"));
			generator.init(Integer.parseInt(p.getProperty("ksSize")));
	        Key encryptionKey = generator.generateKey();
	        
	        return Utils.toHex(encryptionKey.getEncoded());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		}
		
        return null;   
	}

}