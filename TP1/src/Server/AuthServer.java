package Server;

import Utils.NounceChecker;
import Utils.Packet;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Properties;

/**
 * AuthServer outline
 * http://stackoverflow.com/questions/5321003/socket-authentication-server
 * 
 * @author miguel
 * 
 */
public class AuthServer {

	static CipherSuiteManager cipherManager;
	static UserManager userManager;
	private static final String ParametersMessage = "usage: java AuthServer auth_port";
	private static final String CLIENT_ERROR = "Error handling the client.";
	private static final String SSL_PROPERTIES = "configSSL.properties";

	static ServerSocketFactory ssf;
	static SSLServerSocket s;

	private static Properties prop;
	private static String[] enableCipher;

	private static void readConfig() {
		FileInputStream in = null;
		try {
			in = new FileInputStream(SSL_PROPERTIES);
			prop = new Properties();
			prop.load(new FileInputStream(SSL_PROPERTIES));
		} catch (FileNotFoundException e) {
			System.out.println("Can't find file.");
			System.exit(0);
		} catch (IOException e) {
			// e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			System.err.println(ParametersMessage);
			System.exit(0);
		}
		readConfig();
		enableCipher = prop.getProperty("enableCiphersuites").split(",");

		System.setProperty("javax.net.ssl.keyStore",prop.getProperty("sKeystore"));
		System.setProperty("javax.net.ssl.keyStorePassword", prop.getProperty("sKeyPassword"));
//		System.setProperty("javax.net.ssl.trustStrore",prop.getProperty("sTruststore"));

//        System.setProperty("javax.net.debug", "ssl");

        if(prop.getProperty("authType").trim().equals("2Way")){
            System.setProperty("javax.net.ssl.trustStore",
                    prop.getProperty("sTruststore"));
            System.setProperty("javax.net.ssl.trustStorePassword",
                    prop.getProperty("sTrustPassword"));
        }


        System.out.println("Done Inicialize");

		ssf = SSLServerSocketFactory.getDefault();
		s = (SSLServerSocket) ssf.createServerSocket(Integer.parseInt(args[0]));
		s.setEnabledCipherSuites(enableCipher);

        if(prop.getProperty("authType").trim().equals("2Way")){
            s.setNeedClientAuth(true);
        }

        cipherManager = new CipherSuiteManager();
        userManager = new UserManager();
        int id = 1;
		while (true) {
			Socket clientSocket = s.accept();
			ClientServiceThread cliThread = new ClientServiceThread(
					clientSocket, id++, cipherManager, userManager);
			cliThread.start();
		}
	}

}

class ClientServiceThread extends Thread {
	private static final String CLIENT_ERROR = "Error handling the client.";
    Socket clientSocket;
	int clientID = -1;

	CipherSuiteManager cipherManager;
	UserManager userManager;

    private NounceChecker nounceChecker;
	private InputStream is;
	private DataInputStream din;
	private OutputStream os;
	private DataOutputStream dos;
    private long nounceClient;
    private long nounceServer;
    private String usernameClient;
    private String password;

    public ClientServiceThread(Socket clientSocket, int clientID,
			CipherSuiteManager cipherManager, UserManager userManager) {
		super();
		this.clientSocket = clientSocket;
		this.clientID = clientID;
		this.cipherManager = cipherManager;
		this.userManager = userManager;

		try {
			is = clientSocket.getInputStream();
			din = new DataInputStream(is);
			os = clientSocket.getOutputStream();
			dos = new DataOutputStream(os);

            nounceChecker = NounceChecker.getInstance();
			
		} catch (IOException e) {
			System.out.println(CLIENT_ERROR);
		}
	}

	public void run() {
        try {
        	is = clientSocket.getInputStream();
        	os = clientSocket.getOutputStream();

            receive();

            if(userManager.userNameExists(usernameClient) && userManager.getPassword(usernameClient).equalsIgnoreCase(password)){
                step2();
            }

            receive();
            
            clientSocket.close();
        } catch (IOException ioe) {
            // Client disconnected
            ioe.printStackTrace();
        }
	}

    private void receive() throws IOException{

        if (din.readByte() != Packet.VERSION) {
            clientSocket.close();
            return;
        }
        switch (din.readByte()) {
            case Packet.CONTENTTYPE1:
                step1();
                break;
            case Packet.CONTENTTYPE3:
                step3();
                break;
            default:
                clientSocket.close();
                break;
        }
    }

    /*
    Receive P->A :<Cert P>,Username, Ya,Na,{H(H(PWD)||Ya||Na)}KprivP
     */
    private void step1() throws IOException {
        int size = din.readInt();
        byte[] data = new byte[size];
        is.read(data);
        ByteBuffer bb = ByteBuffer.wrap(data);

        nounceClient = bb.getLong();
        if (nounceChecker.checkNouce(nounceClient)){
            clientSocket.close();
            return;
        }

        int sizeUser = bb.getInt();
        byte[] byteUser = new byte[sizeUser];
        bb.get(byteUser);
        usernameClient = new String(byteUser);

        int sizePass = bb.getInt();
        byte[] bytePass = new byte[sizePass];
        bb.get(bytePass);
        password = new String(bytePass);
    }

    /*
    Receive P->A : {Nc+1,<OK_FLAG>}ks
     */
    private void step3() throws IOException {

        nounceServer++;

        //Dados recebidos
        int size = din.readInt();
        byte[] data = new byte[size];
        is.read(data);
        ByteBuffer bb = ByteBuffer.wrap(data);

        long nounceDPlusOne = bb.getLong();

        if(nounceDPlusOne == nounceServer){
            int okFlag = bb.getInt();
            //Authentication process is finished, send a message
            if(okFlag == 1){
                System.out.println("Authentication complete for client: " + clientID);
                clientSocket.close();
            }else{
                System.out.println("Error in the process.");
                throw new IOException("Step 5");
            }
        }
        else{
            throw new IOException("Step 5");
        }
    }

    /*
    Send A->P : {Nb+1,Nc,<Ciphersuite>,H(M)}ks
     */
    private void step2() throws IllegalStateException, IOException{
        byte[] buffer = new byte[65000];
        ByteBuffer bb = ByteBuffer.wrap(buffer);
        int size = 0;

        bb.put(Packet.VERSION);
        bb.put(Packet.CONTENTTYPE2);
        // Reserve space to size package (int 4 bytes)
        bb.putInt(1);
        //8 bytes long
        nounceClient++;
        bb.putLong(nounceClient);
        //8 bytes long
        nounceServer = Packet.getNounce();
        bb.putLong(nounceServer);

        size += 16;

        //---------------------------CipherSuite---------------------------------------------------------
        String total = "provider:" + cipherManager.getProperty("provider") + ";";
        total += "sessionId:" + cipherManager.getProperty("sessionId") + ";";
        total += "ipMultiCast:" + cipherManager.getProperty("ipMultiCast") +";";
        total += "port:"+cipherManager.getProperty("port") +";";
        total += "algorithm:" + cipherManager.getProperty("algorithm")+";";
        total += "mode:" + cipherManager.getProperty("mode")+";";
        total += "padding:" + cipherManager.getProperty("padding")+";";
        total += "key:" + cipherManager.getProperty("key")+";";
        total += "iv:" + cipherManager.getProperty("iv")+";";
        total += "hmacAlgorithm:" + cipherManager.getProperty("hmacAlgorithm")+";";
        total += "hmacKey:" + cipherManager.getProperty("hmacKey");

        byte[] btotal = total.getBytes();

        bb.putInt(btotal.length);
        bb.put(btotal);
        size += btotal.length + 4;
        bb.putInt(2, size);

        dos.write(buffer, 0, size+6);

    }
}
